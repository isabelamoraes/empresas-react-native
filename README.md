# Desafio React Native - ioasys
---

![N|Solid](app-empresa.gif)

## Funcionamento
---
- **SignIn**: permite que o usuário faça login no aplicativo caso informe suas credenciais corretamente.

- **Dashboard**: o cabeçalho dessa tela traz uma mensagem de boas-vindas ao usuário e um botão que permite que ele faça logout do aplicativo. 
Logo abaixo, disponibiliza um botão que, quando pressionado, expande um modal para que o usuário preencha o nome da empresa desejada e o identificador do tipo dela. Clicando em filtrar, o modal será fechado e a aplicação irá atualizar a listagem de empresas de acordo com o resultado obtido.
A listagem de empresas traz previamente algumas informações sobre ela, como a foto, o nome e o tipo. Ao clicar sobre determinada empresa, o usuário será direcionado para uma tela com informações completas sobre ela.

- **Enterprise**: essa tela carrega todas as informações sobre determinada empresa.


## Tecnologias
---
-  **[Expo](https://reactnative.dev/)**: permite desenvolver aplicações mobile com React Native e com o Javascript como linguagem de programação. Para a criação do projeto foi utilizada a configuração Expo Bare Workflow, que além de trazer as facilidades proporcionadas pelo Expo, permite ter autonomia para realizar customização nas pastas nativas (Android e iOS) caso necessário.
-  **[Expo Vector Icons](https://docs.expo.dev/guides/icons/)**: ao utilizar o Expo, é possível usufruir do Expo Vector Icons, uma coleção que reuni várias bibliotecas de ícones. Para essa aplicação foi utilizado o Feather, que apresentava todos os ícones desejados para a aplicação.
-  **[TypeScript](https://reactnative.dev/docs/typescript)**: o TypeScript foi utilizado com o intuito de adicionar tipagem aos componentes da aplicação, de modo a facilitar a manutenção, aumentar a produtividade (IntelliSense)  e evitar erros.
-  **[Expo Font e Expo Google Fonts](https://docs.expo.dev/guides/using-custom-fonts/)**: possibilitou utilizar as fontes Archivo e Inter do Google para personalizar a tipografia da aplicação.
-  **[App Loading](https://docs.expo.dev/versions/latest/sdk/app-loading/)**: recurso utilizado para manter a aplicação na tela de splash enquanto as fonts são carregadas.
-  **[React Navigation](https://reactnavigation.org/)**: essa biblioteca foi utilizada com o intuito de criar a navegação e rotas da aplicação.
-  **[Styled Components](https://styled-components.com/)**: biblioteca de estilização baseada em CSS. Ela foi utilizada devido à flexibilidade e dinamismo que oferece, possibilitando utilizar propriedades com base em estados.
-  **[React Native Responsive Font Size](https://www.npmjs.com/package/react-native-responsive-fontsize)**: a partir dessa biblioteca foi possível utilizar a função RFValeu, que faz o tratamento do valor inserido, possibilitando trabalhar com diferentes proporções.
-  **[React Native SVG](https://github.com/react-native-svg/react-native-svg)** e **[SVG Transformer](https://github.com/kristerkari/react-native-svg-transformer)**: possibilita utilizar images SVG como componentes na aplicação.
-  **[Axios](https://github.com/axios/axios)**: essa biblioteca foi utilizada para lidar com requisição HTTP com a API.

## Instruções
---
Para clonar e executar essa aplicação, execute os seguintes comandos:

```bash
# Clonar o repositório
$ git clone https://bitbucket.org/isabelamoraes/empresas-react-native.git

# Acessar a pasta do projeto
$ cd empresas-react-native/app-empresa

# Instalar as dependências
$ yarn

# Executar o projeto com o expo
$ expo start

# Executar o projeto no emulador
$ yarn android

```

## Design da Aplicação
---
De modo a auxiliar o processo de desenvolvimento do aplicativo, o design foi projetado no Figma e o modelo está disponível para visualização nesse [link](https://www.figma.com/file/BXkOjTvkJeqPc5I73yKIAO/App-Empresa?node-id=5%3A492).
