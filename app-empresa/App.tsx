import React from 'react';
import { useFonts } from 'expo-font';
import { StatusBar } from 'react-native';
import { Inter_400Regular, Inter_700Bold } from '@expo-google-fonts/inter';
import { Archivo_500Medium, Archivo_700Bold } from '@expo-google-fonts/archivo';
import { ThemeProvider } from 'styled-components';
import AppLoading from 'expo-app-loading';

import { AuthProvider } from './src/hooks/auth';

import theme from './src/styles/theme';

import { Routes } from './src/routes';

export default function App() {
  const [fontsLoaded] = useFonts({
    Inter_400Regular,
    Inter_700Bold,
    Archivo_500Medium,
    Archivo_700Bold
  })

  if (!fontsLoaded) {
    return <AppLoading />
  }

  return (
    <ThemeProvider theme={theme}>
      <StatusBar
        barStyle="dark-content"
        translucent
        backgroundColor="transparent"
      />

      <AuthProvider>
        <Routes />
      </AuthProvider>
    </ThemeProvider>
  );
}