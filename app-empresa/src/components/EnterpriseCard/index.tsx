import React from 'react';
import { Feather } from '@expo/vector-icons';
import { useTheme } from 'styled-components';
import { RectButtonProperties } from 'react-native-gesture-handler';


import {
    Container,
    Wrapper,
    Image,
    Info,
    Title,
    Type
} from './styles';

interface Props extends RectButtonProperties{
    id: string;
    name: string;
    photo: string;
    enterprise_type_name: string;
}

export function EnterpriseCard({
    id,
    name,
    photo,
    enterprise_type_name,
    onPress
}: Props) {
    const theme = useTheme();

    return (
        <Container
            onPress={onPress}
        >
            <Wrapper>
                <Image
                    source={{
                        uri: photo
                    }}
                    resizeMode="cover"
                />

                <Info>
                    <Title>{name}</Title>

                    <Type>{enterprise_type_name}</Type>
                </Info>

            </Wrapper>

            <Feather
                name="chevron-right"
                size={24}
                color={theme.colors.primary}
            />
        </Container>
    );
}