import styled from 'styled-components/native';
import { RFValue } from 'react-native-responsive-fontsize';
import { RectButton } from 'react-native-gesture-handler';

export const Container = styled(RectButton)`
    background-color: ${({ theme }) => theme.colors.background};

    flex-direction: row;
    align-items: center;

    justify-content: space-between;

    width: 100%;    
    border-radius: 20px;

    padding: 15px;
    margin-bottom: 15px;
`;

export const Wrapper = styled.View`
    flex-direction: row;
`;

export const Image = styled.Image`
    height: 50px;
    width: 50px;
    border-radius: 15px;
`;

export const Info = styled.View`
    margin-left: 24px;
`;

export const Title = styled.Text`
    font-family: ${({ theme }) => theme.fonts.text_bold};
    font-size: ${RFValue(16)}px;
    color: ${({ theme }) => theme.colors.primary};
`;

export const Type = styled.Text`
    font-family: ${({ theme }) => theme.fonts.text_regular};
    font-size: ${RFValue(14)}px;
    color: ${({ theme }) => theme.colors.primary};
`;
