import React from 'react';
import { Feather } from '@expo/vector-icons';
import { useTheme } from 'styled-components';
import { TextInputProps } from 'react-native';

import {
    Container,
    Icon,
    Input
} from './styles';

interface Props extends TextInputProps {
    iconName: React.ComponentProps<typeof Feather>['name'];
}

export function InputLogin({
    iconName,
    ...rest
}: Props) {
    const theme = useTheme();
    return (
        <Container>

            <Icon>
                <Feather
                    name={iconName}
                    size={24}
                    color={theme.colors.primary}
                />
            </Icon>

            <Input
                placeholderTextColor={theme.colors.text}
                {...rest}
            />
        </Container>
    );
}