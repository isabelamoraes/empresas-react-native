import styled from 'styled-components/native';
import { TextInput } from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';

export const Container = styled.View`
    background-color: ${({ theme }) => theme.colors.secondary};

    flex-direction: row;
    align-items: center;

    height: 56px;
    width: 100%;    
    border-radius: 20px;

    margin-bottom: 15px;
`;

export const Icon = styled.View`
    padding-left: 16px;
    padding-right: 32px;
`;

export const Input = styled(TextInput)`
    width: 80%;

    font-family: ${({ theme }) => theme.fonts.text_regular};
    font-size: ${RFValue(16)}px;
    color: ${({ theme }) => theme.colors.primary}
`;