import styled from 'styled-components/native';
import { RFValue } from 'react-native-responsive-fontsize';
import { RectButton } from 'react-native-gesture-handler';
import { TouchableOpacity } from 'react-native';

export const Container = styled(TouchableOpacity)`
    background-color: ${({ theme }) => theme.colors.primary};

    width: 100%;
    height: 56px; 
    border-radius: 20px;
    
    align-items: center;
    justify-content: center;
`;

export const ButtonText = styled.Text`
    font-family: ${({ theme }) => theme.fonts.title_bold};
    font-size: ${RFValue(18)}px;
    color: ${({ theme }) => theme.colors.background};
`;