import React from 'react';
import { TouchableOpacityProps, ActivityIndicator } from 'react-native';
import { useTheme } from 'styled-components';

import {
    Container,
    ButtonText
} from './styles';

interface Props extends TouchableOpacityProps {
    text: string;
    loading?: boolean;
}

export function Button({
    text,
    loading = false,
    ...rest
}: Props) {

    const theme = useTheme();

    return (
        <Container {...rest} activeOpacity={.6} style={loading && {opacity: .6}} disabled={loading}>
            <ButtonText>
                {loading ?
                    <ActivityIndicator size="small" color={theme.colors.background} />
                    : text}
            </ButtonText>
        </Container>
    );
}