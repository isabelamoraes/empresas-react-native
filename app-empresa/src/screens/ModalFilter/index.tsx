import React, { useState } from 'react';
import { useTheme } from 'styled-components';
import { 
    Alert,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    Keyboard
} from 'react-native';

import {
    Container,
    Content,
    Input,
    CloseModal,
    TextCloseModal
} from './styles';

import { Button } from '../../components/Button';

interface Filter {
    enterprise: string;
    type: string;
}

interface Props {
    setFilter: (item: Filter) => void;
}

export function ModalFilter({
    setFilter
}: Props) {

    const theme = useTheme();

    const [enterprise, setEnterprise] = useState('');
    const [type, setType] = useState('')

    function handleFilter() {

        if (enterprise != '' && type != '') {
            const newFilter = {
                enterprise,
                type
            }

            setFilter(newFilter);

        } else {
            Alert.alert('Atenção', 'É necessário informar o nome da empresa e selecionar o tipo');
        }
    }

    return (
        <Container>
            <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={-30}>
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <Content>
                        <Input
                            placeholderTextColor={theme.colors.text}
                            autoCorrect={false}
                            autoCapitalize="words"
                            placeholder="Nome da empresa"
                            value={enterprise}
                            onChangeText={setEnterprise}
                        />

                        <Input
                            placeholderTextColor={theme.colors.text}
                            autoCorrect={false}
                            keyboardType="numeric"
                            placeholder="Identificador do tipo da empresa"
                            value={type}
                            onChangeText={setType}
                        />

                        <Button
                            onPress={handleFilter}
                            text="FILTRAR"
                        />

                        <CloseModal
                            onPress={() => setFilter({enterprise, type})}
                        >
                            <TextCloseModal>Cancelar</TextCloseModal>
                        </CloseModal>
                    </Content>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        </Container>
    );
}