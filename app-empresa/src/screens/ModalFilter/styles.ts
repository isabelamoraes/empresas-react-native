import styled from 'styled-components/native';
import { TextInput, TouchableOpacity } from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';

export const Container = styled.View`
    flex: 1;
    background-color: #00000050;

    justify-content: flex-end;
`;

export const Content = styled.View`
    width: 100%;
    border-top-left-radius: 30px;
    border-top-right-radius: 30px;

    background-color: ${({ theme }) => theme.colors.background};

    z-index: 1;

    padding: 30px;
    height: 380px;
`;

export const Input = styled(TextInput)`
    font-family: ${({ theme }) => theme.fonts.text_regular};
    font-size: ${RFValue(16)}px;
    color: ${({ theme, value }) => value ? theme.colors.primary : theme.colors.text};
    background-color: ${({ theme }) => theme.colors.secondary};

    padding-left: 16px;

    height: 56px;
    width: 100%;    
    border-radius: 20px;

    margin-bottom: 15px;
`;

export const CloseModal = styled(TouchableOpacity)`
    height: 56px;
    width: 100%;

    align-items: center;
    justify-content: center;
`;

export const TextCloseModal = styled.Text`
    font-family: ${({ theme }) => theme.fonts.text_regular};
    font-size: ${RFValue(16)}px;
    color: ${({ theme }) => theme.colors.text};
`;
