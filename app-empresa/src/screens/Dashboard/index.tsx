import React, { useState, useCallback } from 'react';
import { Feather } from '@expo/vector-icons';
import { useTheme } from 'styled-components';
import { 
    FlatList, 
    Modal, 
    ActivityIndicator, 
    Alert
} from 'react-native';
import { useFocusEffect } from '@react-navigation/native';

import { useAuth } from '../../hooks/auth';

import api from '../../services/api';

import { EnterpriseCard } from '../../components/EnterpriseCard';
import { ModalFilter } from '../ModalFilter';

import {
    Container,
    Header,
    Welcome,
    TextWelcome,
    TextWelcomeBold,
    Logout,
    Filter,
    TextFilter,
    Icon,
    Title,
    Empty
} from './styles';

interface FilterProps {
    enterprise: string;
    type: string;
}

interface EnterpriseType {
    enterprise_type_name: string;
    id: number;
}

interface Enterprise {
    city: string;
    country: string;
    description: string;
    email_enterprise: string;
    enterprise_name: string;
    enterprise_type: EnterpriseType;
    facebook: string;
    id: number;
    linkedin: string;
    own_enterprise: boolean;
    phone: string;
    photo: string;
    share_price: number;
    twitter: string;
    value: number;
}

export function Dashboard({ navigation }: any) {
    const theme = useTheme();

    const { user, signOut } = useAuth();

    const [loading, setLoading] = useState(false);
    const [enterprises, setEnterprises] = useState<Enterprise[]>([]);
    const [filter, setFilter] = useState<FilterProps>({} as FilterProps);
    const [filterModalOpen, setFilterModalOpen] = useState(false);

    function handleOpenModal() {
        setFilterModalOpen(true);
    }

    async function handleCloseModal(item: FilterProps) {
        setFilter(item);
        setFilterModalOpen(false);
        setEnterprises([]);

        if(item.enterprise != '' && item.type != ''){
            await loadDataFilter(item.enterprise, item.type);
        }else{
            loadData();
        }
    }

    function handleNavigateToEnterprise(id: number) {
        navigation.navigate('Enterprise', { id });
    }

    async function loadData() {
        try {
            setLoading(true);

            const response = await api.get(
                'enterprises', {
                    headers: {
                        'Content-Type': 'application/json',
                        'access-token': user.access_token,
                        'client': user.client,
                        'uid': user.uid
                    }
                }
            );

            setEnterprises(response.data.enterprises);

        } catch {
            Alert.alert('Não foi possível carregar os dados')
        } finally {
            setLoading(false);
        }
    }

    async function loadDataFilter(enterprise: string, type: string){
        try {
            setLoading(true);

            const response = await api.get(
                `enterprises?enterprise_types=${type}&name=${enterprise}`, {
                    headers: {
                        'Content-Type': 'application/json',
                        'access-token': user.access_token,
                        'client': user.client,
                        'uid': user.uid
                    }
                }
            );

            setEnterprises(response.data.enterprises);

        } catch {
            Alert.alert('Não foi possível carregar os dados')
        } finally {
            setLoading(false);
        }
    }

    useFocusEffect(useCallback(() => {
        setFilter({} as FilterProps);
		loadData();
	}, []));

    return (
        <Container>
            <Header>
                <Welcome>
                    <TextWelcome>Olá, {user.investor_name}</TextWelcome>

                    <TextWelcomeBold>encontre sua empresa</TextWelcomeBold>
                </Welcome>

                <Logout
                    onPress={signOut}
                >
                    <Feather
                        name="log-out"
                        size={24}
                        color={theme.colors.primary}
                    />
                </Logout>
            </Header>

            <Filter
                onPress={handleOpenModal}
            >
                <TextFilter
                    value={!!filter.enterprise}
                >
                    {!!!filter.enterprise ? 'Filtrar pela empresa e tipo' : `${filter.enterprise} - ${filter.type}`}
                </TextFilter>

                <Icon>
                    <Feather
                        name="sliders"
                        size={24}
                        color={theme.colors.primary}
                    />
                </Icon>
            </Filter>

            <Title>Empresas</Title>

            {
                loading
                    ? <ActivityIndicator size="large" color={theme.colors.primary} />
                    :
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        data={enterprises}
                        keyExtractor={item => String(item.id)}
                        renderItem={({ item }) =>
                            <EnterpriseCard
                                id={String(item.id)}
                                name={item.enterprise_name}
                                photo={`https://empresas.ioasys.com.br/${item.photo}`}
                                enterprise_type_name={item.enterprise_type.enterprise_type_name}
                                onPress={() => handleNavigateToEnterprise(item.id)}
                            />
                        }
                        ListEmptyComponent={(
                            <Empty>Nenhuma empresa encontrada</Empty>
                        )}
                    />
            }

            <Modal
                visible={filterModalOpen}
                transparent
                statusBarTranslucent
                animationType="fade"
            >
                <ModalFilter
                    setFilter={handleCloseModal}
                />
            </Modal>

        </Container>
    );
}