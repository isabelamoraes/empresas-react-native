import styled from 'styled-components/native';
import { TextInput } from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';
import { BorderlessButton, RectButton } from 'react-native-gesture-handler';

interface Props {
    value: boolean;
}

export const Container = styled.View`
    flex: 1;
    background-color: ${({ theme }) => theme.colors.secondary};

    padding:30px;
`;

export const Header = styled.View`
    margin-top: 27px;
    
    flex-direction: row;
    justify-content: space-between;
`;

export const Welcome = styled.View``;

export const TextWelcome = styled.Text`
    font-family: ${({ theme }) => theme.fonts.title_medium};
    font-size: ${RFValue(20)}px;
    color: ${({ theme }) => theme.colors.primary};
`;

export const TextWelcomeBold = styled.Text`
    font-family: ${({ theme }) => theme.fonts.title_bold};
    font-size: ${RFValue(20)}px;
    color: ${({ theme }) => theme.colors.primary};
`;

export const Logout = styled(BorderlessButton)`
    width: 30px;
    height: 30px;

    padding: 3px;
`;

export const Filter = styled(RectButton)`
    background-color: ${({ theme }) => theme.colors.background};

    flex-direction: row;
    align-items: center;

    height: 56px;
    width: 100%;    
    border-radius: 20px;

    margin-top: 40px;
    margin-bottom: 40px;;
`;

export const TextFilter = styled.Text<Props>`
    width: 80%;

    font-family: ${({ theme }) => theme.fonts.text_regular};
    font-size: ${RFValue(16)}px;
    color: ${({ theme, value }) => value ? theme.colors.primary : theme.colors.text};

    padding-left: 16px;
`;

export const Icon = styled.View`
    padding-left: 16px;
    padding-right: 16px;
`;

export const Title = styled.Text`
    font-family: ${({ theme }) => theme.fonts.text_bold};
    font-size: ${RFValue(18)}px;
    color: ${({ theme }) => theme.colors.primary};

    margin-bottom: 32px;
`;

export const Empty = styled.Text`
    font-family: ${({ theme }) => theme.fonts.text_regular};
    font-size: ${RFValue(16)}px;
    color: ${({ theme }) => theme.colors.text};
`;