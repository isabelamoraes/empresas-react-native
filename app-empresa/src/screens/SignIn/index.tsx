import React, { useState } from 'react';
import {
    Alert,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    Keyboard
} from 'react-native';

import {
    Container,
    Content,
    Title,
    Form,
} from './styles';

import SignInSVG from '../../assets/signIn.svg';

import { InputLogin } from '../../components/InputLogin';
import { Button } from '../../components/Button';

import { useAuth } from '../../hooks/auth';

export function SignIn() {
    const { signIn, loading } = useAuth();

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [activedForm, setActivedForm] = useState(false);

    async function handleSignIn() {
        try {
            Keyboard.dismiss();
            setActivedForm(false);

            if (email != '' && password != '') {
                await signIn(email, password);
            } else {
                Alert.alert('Atenção', 'É necessário informar o e-mail e senha para entrar')
            }

        } catch {
            Alert.alert('Erro na autenticação', 'Não foi possível fazer login, verifique suas credenciais');
        }
    }

    function handleCloseForm() {
        Keyboard.dismiss();
        setActivedForm(false);
    }

    return (
        <Container>
            <Content>

                {
                    !activedForm && <SignInSVG />
                }

                <KeyboardAvoidingView behavior="position" onTouchStart={() => setActivedForm(true)}>
                    <TouchableWithoutFeedback onPress={handleCloseForm}>
                        <Form>
                            <Title>
                                Conecte-se e
                                {'\n'}encontre empresas
                            </Title>

                            <InputLogin
                                iconName="user"
                                autoCorrect={false}
                                keyboardType="email-address"
                                placeholder="Digite seu e-mail"
                                autoCapitalize="none"
                                value={email}
                                onChangeText={setEmail}
                            />

                            <InputLogin
                                iconName="lock"
                                autoCorrect={false}
                                keyboardType="default"
                                placeholder="Digite sua senha"
                                secureTextEntry
                                value={password}
                                onChangeText={setPassword}
                            />

                            <Button
                                style={{ marginTop: 32 }}
                                onPress={handleSignIn}
                                loading={loading}
                                text="ENTRAR"
                            />
                        </Form>

                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>
            </Content>
        </Container>
    );
}