import styled from 'styled-components/native';
import { RFValue } from 'react-native-responsive-fontsize';

export const Container = styled.View`
    flex: 1;
    background-color: ${({ theme }) => theme.colors.background};

    padding:30px;
`;

export const Content = styled.View`
    flex: 1;

    align-items: center;
    justify-content: space-between;

    padding-top: 65px;
`;

export const Title = styled.Text`
    font-family: ${({ theme }) => theme.fonts.title_bold};
    font-size: ${RFValue(28)}px;
    color: ${({ theme }) => theme.colors.primary};

    margin-bottom: 32px;
`;

export const Form = styled.View`
    width: 100%;
    background-color: ${({ theme }) => theme.colors.background};
`;