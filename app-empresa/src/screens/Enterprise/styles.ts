import styled from 'styled-components/native';
import { Image } from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';
import { RectButton } from 'react-native-gesture-handler';

export const Container = styled.View`
    flex: 1;
`;

export const HeaderImage = styled(Image)`
    width: 100%;
    height: 240px;
`;

export const Content = styled.ScrollView`
    flex: 1;
    
    border-top-left-radius: 30px;
    border-top-right-radius: 30px;

    background-color: ${({ theme }) => theme.colors.background};

    z-index: 1;

    margin-top: -30px;

    padding: 30px;
`;

export const Title = styled.Text`
    margin-top: 20px;

    font-family: ${({ theme }) => theme.fonts.title_bold};
    font-size: ${RFValue(24)}px;
    color: ${({ theme }) => theme.colors.primary};
`;

export const Type = styled.Text`
    font-family: ${({ theme }) => theme.fonts.text_regular};
    font-size: ${RFValue(20)}px;
    color: ${({ theme }) => theme.colors.text};
`;

export const TitleSmall = styled.Text`
    margin-top: 40px;
    
    font-family: ${({ theme }) => theme.fonts.text_bold};
    font-size: ${RFValue(16)}px;
    color: ${({ theme }) => theme.colors.primary};
`;

export const Text = styled.Text`
    margin-top: 15px;

    font-family: ${({ theme }) => theme.fonts.text_regular};
    font-size: ${RFValue(16)}px;
    color: ${({ theme }) => theme.colors.text};

    text-align: justify;
`;

export const Location = styled.View`
    width: 100%;

    flex-direction: row;

    margin-top: 41px;
    margin-bottom: 41px;
`;

export const TextLocation = styled.Text`
    margin-left: 20px;    

    font-family: ${({ theme }) => theme.fonts.text_regular};
    font-size: ${RFValue(16)}px;
    color: ${({ theme }) => theme.colors.primary};
`;

export const Button = styled(RectButton)`
    background-color: ${({ theme }) => theme.colors.secondary};
    align-items: center;
    justify-content: center;

    height: 56px;
    width: 100%;    
    border-radius: 20px;

    margin-bottom: 40px;;
`;

export const TextButton = styled.Text`

    font-family: ${({ theme }) => theme.fonts.title_bold};
    font-size: ${RFValue(18)}px;
    color: ${({ theme }) => theme.colors.primary};
`;
