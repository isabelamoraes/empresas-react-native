import React, { useState, useCallback } from 'react';
import { Feather } from '@expo/vector-icons';
import { useTheme } from 'styled-components';
import { useRoute, useFocusEffect } from '@react-navigation/native';
import { Alert, ActivityIndicator } from 'react-native';

import { useAuth } from '../../hooks/auth';

import api from '../../services/api';

import {
    Container,
    HeaderImage,
    Content,
    Title,
    Type,
    TitleSmall,
    Text,
    Location,
    TextLocation,
    Button,
    TextButton
} from './styles';

interface EnterpriseType {
    enterprise_type_name: string;
    id: number;
}

interface Enterprise {
    city: string;
    country: string;
    description: string;
    email_enterprise: string;
    enterprise_name: string;
    enterprise_type: EnterpriseType;
    facebook: string;
    id: number;
    linkedin: string;
    own_enterprise: boolean;
    phone: string;
    photo: string;
    share_price: number;
    twitter: string;
    value: number;
}

interface Params {
    id: number
}

export function Enterprise({ navigation }: any) {
    const theme = useTheme();
    const routes = useRoute();

    const { user } = useAuth();

    const { id } = routes.params as Params;

    const [loading, setLoading] = useState(false);
    const [data, setData] = useState<Enterprise>({} as Enterprise);

    async function loadData() {
        try {
            setLoading(true);

            const response = await api.get(
                `enterprises/${id}`, {
                    headers: {
                        'Content-Type': 'application/json',
                        'access-token': user.access_token,
                        'client': user.client,
                        'uid': user.uid
                    }
                }
            );

            setData(response.data.enterprise);

        } catch {
            Alert.alert('Não foi possível carregar os dados')
        } finally {
            setLoading(false)
        }
    }

    useFocusEffect(useCallback(() => {
		loadData();
	}, []));

    return (
        <Container>

            {
                loading
                    ? <ActivityIndicator size="large" color={theme.colors.primary} />
                    :
                    !!data.enterprise_type &&
                    <>
                        <HeaderImage
                            source={{
                                uri: `https://empresas.ioasys.com.br/${data.photo}`
                            }}
                            resizeMode="cover"
                        />

                        <Content
                            showsVerticalScrollIndicator={false}
                        >
                            <Title>{data.enterprise_name}</Title>
                            <Type>{data.enterprise_type.enterprise_type_name}</Type>

                            <TitleSmall>Sobre a empresa</TitleSmall>
                            <Text>{data.description}</Text>

                            <Location>
                                <Feather
                                    name="map-pin"
                                    size={24}
                                    color={theme.colors.primary}
                                />

                                <TextLocation>{data.city} - {data.country}</TextLocation>
                            </Location>

                            <Button onPress={() => navigation.goBack()}>
                                <TextButton>VOLTAR</TextButton>
                            </Button>
                        </Content>
                    </>
            }
        </Container>
    );
}