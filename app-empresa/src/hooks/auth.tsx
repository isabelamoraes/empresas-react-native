import React, { 
    createContext,
    useContext,
    useState,
    ReactNode,
    useEffect
} from 'react';

import api from '../services/api';

interface User {
    id: string,
    investor_name: string;
    access_token: string;
    uid: string;
    client: string;
}

type AuthContextData = {
    user: User;
    loading: boolean;
    signIn: (email: string, password: string) => Promise<void>;
    signOut: () => Promise<void>;
}

type AuthProviderProps = {
    children: ReactNode
}

export const AuthContext = createContext({} as AuthContextData);

function AuthProvider({ children }: AuthProviderProps){
    const [user, setUser] = useState<User>({} as User);
    const [loading, setLoading] = useState(false);

    async function signIn(email: string, password: string){
        try {
            setLoading(true);

            const response = await api.post('users/auth/sign_in', {
                email: email,
                password: password
            });

            const userLogged = {
                id: response.data.investor.id,
                investor_name: response.data.investor.investor_name,
                access_token: response.headers['access-token'],
                uid: response.headers['uid'],
                client: response.headers['client'],
            }
            
            setUser(userLogged)
        } catch {
            throw new Error('Não foi possível autenticar');
        } finally {
            setLoading(false);
        }
    }

    async function signOut(){
        setUser({} as User);
    }

    return (
        <AuthContext.Provider value={{
            user,
            loading,
            signIn,
            signOut
        }}>
            { children }
        </AuthContext.Provider>
    )
}

function useAuth(){
    const context = useContext(AuthContext);

    return context;
}

export {
    AuthProvider,
    useAuth
}