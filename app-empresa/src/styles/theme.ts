export default {
    colors: {
      primary: '#000000',
      secondary: '#F7F7F7',
      background: '#FFFFFF',
      text: '#ABABAB',
    },
  
    fonts: {
      title_medium: 'Archivo_500Medium',
      title_bold: 'Archivo_700Bold',
      text_regular: 'Inter_400Regular',
      text_bold: 'Inter_700Bold'
    }
  };