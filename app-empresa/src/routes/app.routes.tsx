import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import { Dashboard } from '../screens/Dashboard';
import { Enterprise } from '../screens/Enterprise';

const { Navigator, Screen } = createNativeStackNavigator();

export function AppRoutes(){
    return(
        <Navigator screenOptions={{ headerShown: false}}>
            <Screen 
                name="Dashboard"
                component={Dashboard}
            />
            <Screen 
                name="Enterprise"
                component={Enterprise}
            />
        </Navigator>
    )
}